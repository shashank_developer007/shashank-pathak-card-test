import './App.css';
import CardScreen from './components/CardScreen';
import Header from './components/Header';

function App() {
  return (
    <div>
      <Header />
     <CardScreen />  
    </div>
  );
}

export default App;
