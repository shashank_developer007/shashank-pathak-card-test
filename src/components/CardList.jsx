import React from 'react'
import {Grid} from '@material-ui/core';
import Cards from './Cards'
import Utils from "../utils"

const cardsContent = [
    {
        id: 1, 
        caption: 'HIRABLE ROOMS',
        Title: 'Anything Room at Green Square Library',
        content: 'Zetland',
        image:` ${Utils.LocalImages.CardImage}`
    },
    {
        id: 2, 
        caption: 'HIRABLE ROOMS',
        Title: 'Anything Room at Green Square Library',
        content: 'Zetland',
        image: ` ${Utils.LocalImages.CardImage}`
    },
    {
        id: 3, 
        caption: 'HIRABLE ROOMS',
        Title: 'Anything Room at Green Square Library',
        content: 'Zetland',
        image: ` ${Utils.LocalImages.CardImage}`
    },
    {
        id: 4, 
        caption: 'HIRABLE ROOMS',
        Title: 'Anything Room at Green Square Library',
        content: 'Zetland',
        image: ` ${Utils.LocalImages.CardImage}`
    },
    {
        id: 5, 
        caption: 'HIRABLE ROOMS',
        Title: 'Anything Room at Green Square Library',
        content: 'Zetland',
        image: ` ${Utils.LocalImages.CardImage}`
    },
    {
        id: 6, 
        caption: 'HIRABLE ROOMS',
        Title: 'Anything Room at Green Square Library',
        content: 'Zetland',
        image: ` ${Utils.LocalImages.CardImage}`
    },
]
const CardList = (props) => {
    return (
        <div>
        <Grid container spacing={3}>
            {cardsContent.map((item) => (
                <Grid item xs={12} sm={6} md={6} lg={4} key={item.id}>
                <a href="https://www.google.com/" target='blank' style={{textDecoration:'none'}}>
                    <Cards 
                    caption={item.caption}
                    Title={item.Title}
                    content={item.content}
                    image={ props.image ? item.image: "" }
                    
                    />
                </a>
                </Grid>
            ))}
                   
            </Grid>
            
        </div>
    )
}

export default CardList
