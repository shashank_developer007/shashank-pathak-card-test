import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import {
    Card, 
    CardActionArea, 
    CardContent, 
    CardMedia,
    Typography 
} from '@material-ui/core';

const useStyles = makeStyles({
    root: {
      maxWidth: '90%',
    },
    media: {
      height: 140,
    },
  });

const Cards = (props) => {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              image={props.image}
              title="Contemplative Reptile"
            />
            <CardContent>
              <Typography gutterBottom variant="subtitle2" color="textSecondary">
                {props.caption}
              </Typography>
              <Typography variant="h5" gutterBottom>
                {props.Title}
              </Typography>
              <Typography variant="h6" color="textSecondary" gutterBottom>
                {props.content}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      );
}

export default Cards
