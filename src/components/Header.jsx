import { Typography, Divider } from '@material-ui/core'
import React from 'react'

const Header = () => {
    return (
        <div>
            <Typography variant="h4" style={{textAlign: 'center', margin: '2rem'}}>Responsive Cards</Typography>
            <Divider />
        </div>
    )
}

export default Header
