import React from 'react'
import {Divider, Grid, Typography} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';


import CardList from './CardList';


const useStyles = makeStyles((theme) => ({
    root: {
        overflow: 'hidden',
        margin: '2rem'
    },
    container: {
        justifyContent: 'space-evenly',
        margin: '1.5rem',
        [theme.breakpoints.down('sm')] : {
            margin: '0.50rem'
        }
    },
    heading: {
        margin: '1.5rem'
    },
    divider: {
        marginTop: '4rem',
        marginBottom: '4rem'
    }

  }));


const CardScreen = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Typography variant="h4" className={classes.heading}>Card With Images</Typography>
            <Grid container className={classes.container}>
                <CardList image={true}/>   
            </Grid>
            <Divider className={classes.divider}/>
            <Typography variant="h4" className={classes.heading}>Card Without Images</Typography>
            <Grid container className={classes.container}>
                <CardList image={false} />   
            </Grid>
        </div>
    )
}

export default CardScreen
